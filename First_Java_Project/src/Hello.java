/*25-NOV-2016 - Tihomir Mladenov - First Java program
 * This simple Java program was written  to  practice 
 * user console input.								*/

import java.io.Console;
import java.util.Scanner;


public class Hello {

	
	public static void main(String[] args) {
		System.out.println("Enter a:");
		
		int a;
		Scanner scanner = new Scanner(System.in);
		String input = scanner.nextLine();
		a = Integer.parseInt(input);
		
		System.out.println("Enter b:");
		int b;
		Scanner scanner2 = new Scanner(System.in);
		String input2 = scanner.nextLine();
		b = Integer.parseInt(input2);
		
		int c = a + b;
		
		System.out.println("Result is: ");
		System.out.println(c);
		
		int[] someArray;
		someArray = new int[10];
		
		for(int i = 0; i < 10; i++) {
			someArray[i] = c + i;
		}
		
		for(int i = 0; i < 10; i++) {
			System.out.println("Array elements:");
			System.out.println(someArray[i]);
		}
		
		int max = someArray[0];
		
		for(int i = 1; i < 10; i++) {
			if(max < someArray[i]) {
				max = someArray[i];
			}
		}
		
		System.out.println("The max element of the array is: ");
		System.out.println(max);
		
		//System.out.println("Hello, World!");
		//System.out.println("My first program!");

	}

}
